ffc.tensor package
==================

Submodules
----------

ffc.tensor.costestimation module
--------------------------------

.. automodule:: ffc.tensor.costestimation
    :members:
    :undoc-members:
    :show-inheritance:

ffc.tensor.geometrytensor module
--------------------------------

.. automodule:: ffc.tensor.geometrytensor
    :members:
    :undoc-members:
    :show-inheritance:

ffc.tensor.monomialextraction module
------------------------------------

.. automodule:: ffc.tensor.monomialextraction
    :members:
    :undoc-members:
    :show-inheritance:

ffc.tensor.monomialintegration module
-------------------------------------

.. automodule:: ffc.tensor.monomialintegration
    :members:
    :undoc-members:
    :show-inheritance:

ffc.tensor.monomialtransformation module
----------------------------------------

.. automodule:: ffc.tensor.monomialtransformation
    :members:
    :undoc-members:
    :show-inheritance:

ffc.tensor.multiindex module
----------------------------

.. automodule:: ffc.tensor.multiindex
    :members:
    :undoc-members:
    :show-inheritance:

ffc.tensor.referencetensor module
---------------------------------

.. automodule:: ffc.tensor.referencetensor
    :members:
    :undoc-members:
    :show-inheritance:

ffc.tensor.tensorgenerator module
---------------------------------

.. automodule:: ffc.tensor.tensorgenerator
    :members:
    :undoc-members:
    :show-inheritance:

ffc.tensor.tensorreordering module
----------------------------------

.. automodule:: ffc.tensor.tensorreordering
    :members:
    :undoc-members:
    :show-inheritance:

ffc.tensor.tensorrepresentation module
--------------------------------------

.. automodule:: ffc.tensor.tensorrepresentation
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: ffc.tensor
    :members:
    :undoc-members:
    :show-inheritance:
