ffc.backends.ufc package
========================

Submodules
----------

ffc.backends.ufc.build module
-----------------------------

.. automodule:: ffc.backends.ufc.build
    :members:
    :undoc-members:
    :show-inheritance:

ffc.backends.ufc.coordinate_mapping module
------------------------------------------

.. automodule:: ffc.backends.ufc.coordinate_mapping
    :members:
    :undoc-members:
    :show-inheritance:

ffc.backends.ufc.dofmap module
------------------------------

.. automodule:: ffc.backends.ufc.dofmap
    :members:
    :undoc-members:
    :show-inheritance:

ffc.backends.ufc.factory module
-------------------------------

.. automodule:: ffc.backends.ufc.factory
    :members:
    :undoc-members:
    :show-inheritance:

ffc.backends.ufc.finite_element module
--------------------------------------

.. automodule:: ffc.backends.ufc.finite_element
    :members:
    :undoc-members:
    :show-inheritance:

ffc.backends.ufc.form module
----------------------------

.. automodule:: ffc.backends.ufc.form
    :members:
    :undoc-members:
    :show-inheritance:

ffc.backends.ufc.function module
--------------------------------

.. automodule:: ffc.backends.ufc.function
    :members:
    :undoc-members:
    :show-inheritance:

ffc.backends.ufc.integrals module
---------------------------------

.. automodule:: ffc.backends.ufc.integrals
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: ffc.backends.ufc
    :members:
    :undoc-members:
    :show-inheritance:
