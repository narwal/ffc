.. title:: FEniCS Form Compiler


FEniCS Form Compiler (FFC)
==========================

This is the documentation for the FEniCS Form Compiler (FFC) from the
FEniCS Project (http://fenicsproject.org).  FFC is a compiler for
finite element variational forms. From a high-level description of the
form, it generates efficient low-level C++ code that can be used to
assemble the corresponding discrete operator (tensor). In particular,
a bilinear form may be assembled into a matrix and a linear form may
be assembled into a vector.


Installation
------------

To install FFC::

  TODO

Help and support
----------------

Send help requests and questions to fenics-support@googlegroups.com,
and send feature requests and questions to
fenics-dev@googlegroups.com.


Development and reporting bugs
------------------------------

The git source repository for FFC is located at
https://bitbucket.org/fenics-project/ffc.

Bugs can be registered at
https://bitbucket.org/fenics-project/ffc/issues.  For general FFC
development questions and to make feature requests, use
fenics-dev@googlegroups.com.



Documentation
-------------

.. toctree::
   :titlesonly:

   api-doc/ffc
   api-doc/uflacs
   releases


* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
