ffc.quadrature package
======================

Submodules
----------

ffc.quadrature.expr module
--------------------------

.. automodule:: ffc.quadrature.expr
    :members:
    :undoc-members:
    :show-inheritance:

ffc.quadrature.floatvalue module
--------------------------------

.. automodule:: ffc.quadrature.floatvalue
    :members:
    :undoc-members:
    :show-inheritance:

ffc.quadrature.fraction module
------------------------------

.. automodule:: ffc.quadrature.fraction
    :members:
    :undoc-members:
    :show-inheritance:

ffc.quadrature.optimisedquadraturetransformer module
----------------------------------------------------

.. automodule:: ffc.quadrature.optimisedquadraturetransformer
    :members:
    :undoc-members:
    :show-inheritance:

ffc.quadrature.parameters module
--------------------------------

.. automodule:: ffc.quadrature.parameters
    :members:
    :undoc-members:
    :show-inheritance:

ffc.quadrature.product module
-----------------------------

.. automodule:: ffc.quadrature.product
    :members:
    :undoc-members:
    :show-inheritance:

ffc.quadrature.quadraturegenerator module
-----------------------------------------

.. automodule:: ffc.quadrature.quadraturegenerator
    :members:
    :undoc-members:
    :show-inheritance:

ffc.quadrature.quadratureoptimization module
--------------------------------------------

.. automodule:: ffc.quadrature.quadratureoptimization
    :members:
    :undoc-members:
    :show-inheritance:

ffc.quadrature.quadraturerepresentation module
----------------------------------------------

.. automodule:: ffc.quadrature.quadraturerepresentation
    :members:
    :undoc-members:
    :show-inheritance:

ffc.quadrature.quadraturetransformer module
-------------------------------------------

.. automodule:: ffc.quadrature.quadraturetransformer
    :members:
    :undoc-members:
    :show-inheritance:

ffc.quadrature.quadraturetransformerbase module
-----------------------------------------------

.. automodule:: ffc.quadrature.quadraturetransformerbase
    :members:
    :undoc-members:
    :show-inheritance:

ffc.quadrature.quadratureutils module
-------------------------------------

.. automodule:: ffc.quadrature.quadratureutils
    :members:
    :undoc-members:
    :show-inheritance:

ffc.quadrature.reduce_operations module
---------------------------------------

.. automodule:: ffc.quadrature.reduce_operations
    :members:
    :undoc-members:
    :show-inheritance:

ffc.quadrature.sumobj module
----------------------------

.. automodule:: ffc.quadrature.sumobj
    :members:
    :undoc-members:
    :show-inheritance:

ffc.quadrature.symbol module
----------------------------

.. automodule:: ffc.quadrature.symbol
    :members:
    :undoc-members:
    :show-inheritance:

ffc.quadrature.symbolics module
-------------------------------

.. automodule:: ffc.quadrature.symbolics
    :members:
    :undoc-members:
    :show-inheritance:

ffc.quadrature.tabulate_basis module
------------------------------------

.. automodule:: ffc.quadrature.tabulate_basis
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: ffc.quadrature
    :members:
    :undoc-members:
    :show-inheritance:
