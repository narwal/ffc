ffc package
===========

Subpackages
-----------

.. toctree::

    ffc.backends
    ffc.errorcontrol
    ffc.quadrature
    ffc.tensor
    ffc.uflacsrepr

Submodules
----------

ffc.analysis module
-------------------

.. automodule:: ffc.analysis
    :members:
    :undoc-members:
    :show-inheritance:

ffc.codegeneration module
-------------------------

.. automodule:: ffc.codegeneration
    :members:
    :undoc-members:
    :show-inheritance:

ffc.codesnippets module
-----------------------

.. automodule:: ffc.codesnippets
    :members:
    :undoc-members:
    :show-inheritance:

ffc.compiler module
-------------------

.. automodule:: ffc.compiler
    :members:
    :undoc-members:
    :show-inheritance:

ffc.cpp module
--------------

.. automodule:: ffc.cpp
    :members:
    :undoc-members:
    :show-inheritance:

ffc.enrichedelement module
--------------------------

.. automodule:: ffc.enrichedelement
    :members:
    :undoc-members:
    :show-inheritance:

ffc.evaluatebasis module
------------------------

.. automodule:: ffc.evaluatebasis
    :members:
    :undoc-members:
    :show-inheritance:

ffc.evaluatebasisderivatives module
-----------------------------------

.. automodule:: ffc.evaluatebasisderivatives
    :members:
    :undoc-members:
    :show-inheritance:

ffc.evaluatedof module
----------------------

.. automodule:: ffc.evaluatedof
    :members:
    :undoc-members:
    :show-inheritance:

ffc.extras module
-----------------

.. automodule:: ffc.extras
    :members:
    :undoc-members:
    :show-inheritance:

ffc.fiatinterface module
------------------------

.. automodule:: ffc.fiatinterface
    :members:
    :undoc-members:
    :show-inheritance:

ffc.formatting module
---------------------

.. automodule:: ffc.formatting
    :members:
    :undoc-members:
    :show-inheritance:

ffc.git_commit_hash module
--------------------------

.. automodule:: ffc.git_commit_hash
    :members:
    :undoc-members:
    :show-inheritance:

ffc.interpolatevertexvalues module
----------------------------------

.. automodule:: ffc.interpolatevertexvalues
    :members:
    :undoc-members:
    :show-inheritance:

ffc.jitcompiler module
----------------------

.. automodule:: ffc.jitcompiler
    :members:
    :undoc-members:
    :show-inheritance:

ffc.jitobject module
--------------------

.. automodule:: ffc.jitobject
    :members:
    :undoc-members:
    :show-inheritance:

ffc.log module
--------------

.. automodule:: ffc.log
    :members:
    :undoc-members:
    :show-inheritance:

ffc.mixedelement module
-----------------------

.. automodule:: ffc.mixedelement
    :members:
    :undoc-members:
    :show-inheritance:

ffc.optimization module
-----------------------

.. automodule:: ffc.optimization
    :members:
    :undoc-members:
    :show-inheritance:

ffc.parameters module
---------------------

.. automodule:: ffc.parameters
    :members:
    :undoc-members:
    :show-inheritance:

ffc.plot module
---------------

.. automodule:: ffc.plot
    :members:
    :undoc-members:
    :show-inheritance:

ffc.quadrature_schemes module
-----------------------------

.. automodule:: ffc.quadrature_schemes
    :members:
    :undoc-members:
    :show-inheritance:

ffc.quadratureelement module
----------------------------

.. automodule:: ffc.quadratureelement
    :members:
    :undoc-members:
    :show-inheritance:

ffc.representation module
-------------------------

.. automodule:: ffc.representation
    :members:
    :undoc-members:
    :show-inheritance:

ffc.representationutils module
------------------------------

.. automodule:: ffc.representationutils
    :members:
    :undoc-members:
    :show-inheritance:

ffc.restrictedelement module
----------------------------

.. automodule:: ffc.restrictedelement
    :members:
    :undoc-members:
    :show-inheritance:

ffc.ufc_signature module
------------------------

.. automodule:: ffc.ufc_signature
    :members:
    :undoc-members:
    :show-inheritance:

ffc.utils module
----------------

.. automodule:: ffc.utils
    :members:
    :undoc-members:
    :show-inheritance:

ffc.wrappers module
-------------------

.. automodule:: ffc.wrappers
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: ffc
    :members:
    :undoc-members:
    :show-inheritance:
