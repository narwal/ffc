ffc.backends.dolfin package
===========================

Submodules
----------

ffc.backends.dolfin.capsules module
-----------------------------------

.. automodule:: ffc.backends.dolfin.capsules
    :members:
    :undoc-members:
    :show-inheritance:

ffc.backends.dolfin.form module
-------------------------------

.. automodule:: ffc.backends.dolfin.form
    :members:
    :undoc-members:
    :show-inheritance:

ffc.backends.dolfin.functionspace module
----------------------------------------

.. automodule:: ffc.backends.dolfin.functionspace
    :members:
    :undoc-members:
    :show-inheritance:

ffc.backends.dolfin.goalfunctional module
-----------------------------------------

.. automodule:: ffc.backends.dolfin.goalfunctional
    :members:
    :undoc-members:
    :show-inheritance:

ffc.backends.dolfin.includes module
-----------------------------------

.. automodule:: ffc.backends.dolfin.includes
    :members:
    :undoc-members:
    :show-inheritance:

ffc.backends.dolfin.wrappers module
-----------------------------------

.. automodule:: ffc.backends.dolfin.wrappers
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: ffc.backends.dolfin
    :members:
    :undoc-members:
    :show-inheritance:
