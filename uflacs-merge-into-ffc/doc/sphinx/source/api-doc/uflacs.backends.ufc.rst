uflacs.backends.ufc package
===========================

Submodules
----------

uflacs.backends.ufc.coordinate_mapping module
---------------------------------------------

.. automodule:: uflacs.backends.ufc.coordinate_mapping
    :members:
    :undoc-members:
    :show-inheritance:

uflacs.backends.ufc.dofmap module
---------------------------------

.. automodule:: uflacs.backends.ufc.dofmap
    :members:
    :undoc-members:
    :show-inheritance:

uflacs.backends.ufc.evaluatebasis module
----------------------------------------

.. automodule:: uflacs.backends.ufc.evaluatebasis
    :members:
    :undoc-members:
    :show-inheritance:

uflacs.backends.ufc.finite_element module
-----------------------------------------

.. automodule:: uflacs.backends.ufc.finite_element
    :members:
    :undoc-members:
    :show-inheritance:

uflacs.backends.ufc.form module
-------------------------------

.. automodule:: uflacs.backends.ufc.form
    :members:
    :undoc-members:
    :show-inheritance:

uflacs.backends.ufc.generator module
------------------------------------

.. automodule:: uflacs.backends.ufc.generator
    :members:
    :undoc-members:
    :show-inheritance:

uflacs.backends.ufc.generators module
-------------------------------------

.. automodule:: uflacs.backends.ufc.generators
    :members:
    :undoc-members:
    :show-inheritance:

uflacs.backends.ufc.integrals module
------------------------------------

.. automodule:: uflacs.backends.ufc.integrals
    :members:
    :undoc-members:
    :show-inheritance:

uflacs.backends.ufc.templates module
------------------------------------

.. automodule:: uflacs.backends.ufc.templates
    :members:
    :undoc-members:
    :show-inheritance:

uflacs.backends.ufc.utils module
--------------------------------

.. automodule:: uflacs.backends.ufc.utils
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: uflacs.backends.ufc
    :members:
    :undoc-members:
    :show-inheritance:
