ffc.errorcontrol package
========================

Submodules
----------

ffc.errorcontrol.errorcontrol module
------------------------------------

.. automodule:: ffc.errorcontrol.errorcontrol
    :members:
    :undoc-members:
    :show-inheritance:

ffc.errorcontrol.errorcontrolgenerators module
----------------------------------------------

.. automodule:: ffc.errorcontrol.errorcontrolgenerators
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: ffc.errorcontrol
    :members:
    :undoc-members:
    :show-inheritance:
